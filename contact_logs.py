import os
import logging
from logging.handlers import TimedRotatingFileHandler
from datetime import datetime

def get_time_date():
    full_date = datetime.now()
    return full_date.strftime('%Y-%m-%dT%H:%M:%S.%fZ')


class contact_logger(logging.Logger):
    def __init__(self, name, log_dir, level=logging.NOTSET):
        super().__init__(name, level)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # Create the log directory if it doesn't exist
        if not os.path.exists(log_dir):
            print("log directory doesn't exist : Creating a new one")
            os.makedirs(log_dir)

        # Create a TimedRotatingFileHandler for daily log rotation
        log_file = os.path.join(log_dir, datetime.now().strftime('%Y-%m-%d.log'))
        file_handler = TimedRotatingFileHandler(log_file, when='midnight', interval=1,
                                                backupCount=7)  # Keep logs for 7 days
        file_handler.setFormatter(formatter)
        self.addHandler(file_handler)

    def logger_debug(self, message: str):
        self.log(logging.DEBUG, f"[DEBUG] {message}")

    def logger_info(self, message: str):
        self.log(logging.INFO, f"{message}")

    def logger_warning(self, message: str):
        self.log(logging.WARNING, f"{message}")

    def logger_error(self, message: str):
        self.log(logging.ERROR, f"{message}")

    def logger_critical(self, message: str):
        self.log(logging.CRITICAL, f"{message}")
        
                            
