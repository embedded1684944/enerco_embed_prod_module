from __future__ import absolute_import
import json
import queue
import time
import subprocess
import kafka.errors
from kafka import KafkaProducer
from kafka.errors import KafkaError, KafkaTimeoutError
from contact_logs import *

def server_check(_addr: str):
    try:
        subprocess.check_output(["ping", "-c", "1", _addr])
        print('\t----------------------\n----------- Server ON! -----------\n\t----------------------')
        return True
    except subprocess.CalledProcessError:
        print('\t----------------------\n----------- Server OFF! -----------\n\t----------------------')
        return False

class kafka_producer:
	@staticmethod
	def json_serializer(data):
		return json.dumps(data).encode('utf-8')

	def __init__(self, server_ip:str, kafka_topic:str, log_dir:str):

		print("Launching Kafka Producer ... ")
		self.server_ip = server_ip
		self.kafka_logger = contact_logger('kafka_logger', log_dir)

		self.bootstrap_server = self.server_ip+':9092'
		self.kafka_topic = kafka_topic
		self.value_serializer = lambda x: self.json_serializer(x)

		self.producer_agent = None
		self.broker_state = False
		self.kafka_future = None
		self.kafka_msg = None

	def create_kafka_producer(self):
		while not server_check(self.server_ip):
			print("Server IP Address Not Responding. Retrying in 1 second...")
			self.kafka_logger.logger_critical("Server IP Address Not Responding. Retrying in 1 second...")
			time.sleep(1)
		print("Server {} Online!".format(self.server_ip))
		self.kafka_logger.logger_info("Server {} Online!".format(self.server_ip))
		while not self.broker_state:
			print("Establishing kafka producer agent, Stand by ...")
			try:
				self.producer_agent = KafkaProducer(
					bootstrap_servers=self.bootstrap_server,
					value_serializer=self.value_serializer
				)
				self.broker_state = True
				time.sleep(1)
				print("kafka producer agent established!")
				self.kafka_logger.logger_info("kafka producer agent established!")
			except kafka.errors.NoBrokersAvailable as n_b_a:
				print(n_b_a)
				self.kafka_logger.logger_error("{}. Retrying in 1 second ...".format(n_b_a))
			time.sleep(1)

	def update_kafka_message(self, message):
		self.kafka_msg = message

	def on_send_success(self, record_metadata):
		self.broker_state = True
		self.kafka_logger.logger_info("Success acknowledgedment : Topic -> {} - Partition -> {} - Offset -> {} - Message -> {}".format(record_metadata.topic, record_metadata.partition, record_metadata.offset, self.kafka_msg))

	def on_send_error(self, exception):
		self.broker_state = False
		self.kafka_logger.logger_error("Failed acknowledgement by kafka broker {} -  message {}".format(exception, self.kafka_msg))

	def produce_message(self):
		try:
		    self.kafka_future=self.producer_agent.send(self.kafka_topic, self.kafka_msg)
		    #self.producer_agent.flush()
		except kafka.errors.NoBrokersAvailable as n_b_a:
			print(n_b_a)
			self.kafka_logger.logger_error("{}".format(n_b_a))
		except KafkaTimeoutError as k_t_e:
			print(k_t_e)
			self.kakfa_logger.logger_error("{}".format(k_t_e))
		except KafkaError as k_e:
			print(k_e)
			self.kafka_logger.logger_error("{}".format(k_e))
		except AttributeError as attr_err:
			print(attr_err)
			self.kafka_logger.logger_error("{}".format(attr_err))
		except Exception as err:
			print(err)
			self.kafka_logger.logger_error("{}".format(err))
		finally:
                    #print(self.kafka_future)
		    self.kafka_future.add_callback(self.on_send_success)
		    self.kafka_future.add_errback(self.on_send_error)

