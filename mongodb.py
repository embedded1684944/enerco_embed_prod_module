import time
import pymongo
from bson import ObjectId
from pymongo import errors
from contact_logs import *
from kafka import *
from bson.json_util import dumps
from bson.json_util import loads

class mongodb:

	@staticmethod
	def test_database_connection(uri: str, logger: contact_logger, auth: bool, db_username, db_password):
		try:
			if auth:
				client = pymongo.MongoClient(uri, username=db_username, password=db_password)
			else:
				client = pymongo.MongoClient(uri)
			#client.admin.command('ping')
			return client
		except errors.ConnectionFailure as err:
			print(err)
			logger.logger_error("{}".format(err))
			return None

	def __init__(self, server_ip:str, mongodb_port: str, username, password,auth:bool, machine_id: str, log_dir):
		self.server_ip = server_ip
		self.mongodb_port = mongodb_port
		self.uri = 'mongodb://' + self.server_ip + ':' + self.mongodb_port + '/'
		self.machine_id = machine_id
		self.mongodb_logger = contact_logger('mongodb_logger', log_dir)

		self.client = self.test_database_connection(self.uri, self.mongodb_logger,auth, username, password)

		self.client_db = self.client["contact_sec"]
		self.col_machines = self.client_db["machines"]
		self.col = self.client_db["settings"]
		self.id = self.col_machines.find_one({"machine_id": self.machine_id}).get("_id")
		self.counter = self.col.find_one({"counter_name":"compteur machine", "machine":ObjectId(self.id)})
		self.pompe_counter = self.counter.get("counter_value")

		self.mongodb_logger.logger_info("Initial Pompe Counter = {}".format(self.pompe_counter))

	def get_latest_pompe_counter(self):
		self.counter = self.col.find_one({"counter_name": "compteur machine", "machine": ObjectId(self.id)})
		self.pompe_counter = self.counter.get("counter_value")
		return self.pompe_counter

#test_db = mongodb("192.168.2.63", "27017", "electrosoft_prod","electrosoft@2023", True, "u4d1m1", "/home/pi/Desktop/embedded_production_monitoring/mongodb_logs/")

#print(test_db.get_latest_pompe_counter())
