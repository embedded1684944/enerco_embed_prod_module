import RPi.GPIO as GPIO
import time
import threading
from contact_logs import *


class pompe_state:

    def __init__(self, log_dir):

        self.pompe_chrono = 9
        self.pompe_gpio = 11
        self.pompe_timer = 0.0
        self.time_date = None
        self.machine_pompe_state = False
        self.pompe_pack = {"counter_to_increment": 0}
        
        self.pompe_logger = contact_logger("pompe_logger", log_dir)
        self.pompe_callback = []

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pompe_gpio, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(self.pompe_gpio, GPIO.BOTH, callback=self.handle_pompe, bouncetime=500)

        if GPIO.input(self.pompe_gpio) == 1:
            self.pompe_timer = float("{:.4f}".format(time.time()))
            self.machine_pompe_state = True
        else:
            self.machine_pompe_state = False
        
        print("Initial Pompe State : {}".format(self.machine_pompe_state))
        self.pompe_logger.logger_info("Initial Pompe State : {}".format(self.machine_pompe_state))
        self.pompe_state = {"state": self.machine_pompe_state, "time_date":get_time_date()}
        #self.start_pompe_task()

    def update_initial_pompe_counter(self, initial_pompe_counter):
        self.pompe_pack.update({"counter_to_increment": initial_pompe_counter})
        print("Initial Pompe Counter {}".format(self.pompe_pack))
        self.pompe_logger.logger_info("Initial Pompe Counter {}".format(self.pompe_pack))

    def start_pompe_task(self):
        pompe_task_thread = threading.Thread(target=self.pompe_task)
        pompe_task_thread.daemon = True
        pompe_task_thread.start()

    def handle_pompe(self,channel):
        self.time_date = get_time_date()
        self.machine_pompe_state = GPIO.input(self.pompe_gpio)
        self.pompe_state.update({"state":self.machine_pompe_state, "time_date": self.time_date})

        for callback in self.pompe_callback:
            callback(self.pompe_pack, self.pompe_state, "pompe_state_pack")
            
        self.pompe_logger.logger_info("Pompe State : {}".format(self.machine_pompe_state))
        
        if self.machine_pompe_state == 1:
            self.pompe_timer = float("{:.4f}".format(time.time()))
        else:
            initial_pompe_counter = self.pompe_pack.get("counter_to_increment")
            counter_to_increment = initial_pompe_counter + ((float("{:.4f}".format(time.time())) - self.pompe_timer)/3600)
            self.pompe_pack.update({"counter_to_increment":float("{:.4f}".format(counter_to_increment))})
            for callback in self.pompe_callback:
                callback(self.pompe_pack, self.pompe_state, "pompe_pack")
            self.pompe_logger.logger_info("Pompe Counter : {}".format(self.pompe_pack))

    def pompe_task(self):
        while True:
            actual_time = float("{:.4f}".format(time.time()))
            if self.machine_pompe_state == 1:
                pompe_timestamp = actual_time - self.pompe_timer
                if pompe_timestamp >= self.pompe_chrono:
                    initial_pompe_counter = self.pompe_pack.get("counter_to_increment")
                    counter_to_increment = initial_pompe_counter + 0.0025
                    self.pompe_pack.update({"counter_to_increment": float("{:.4f}".format(counter_to_increment))})
                    for callback in self.pompe_callback:
                        callback(self.pompe_pack, self.pompe_state, "pompe_pack")
                    self.pompe_logger.logger_info("New Machine Counter {}".format(self.pompe_pack))
                    self.pompe_timer = actual_time

    def register_pompe_callback(self, callback):
        self.pompe_callback.append(callback)

    def unregister_pompe_callback(self, callback):
        if callback in self.pompe_callback:
            self.pompe_callback.remove(callback)

def prototype_pompe_callback(pompe_pack, pompe_state_pack, pompe_condition:str):
    if pompe_condition == "pompe_pack":
        print("New pompe Counter Value : {}".format(pompe_pack))
    elif pompe_condition == "pompe_state_pack":
        print("New pompe State : {}".format(pompe_state_pack))
    else:
        print("ERROR: pompe_condition!")

def prototype_pompe_remove_callback():
    pass

"""
log_dir = "/home/pi/Desktop/test_contact_sec/pompe_logs"

pompe_monitor = pompe_state(log_dir)
pompe_monitor.update_initial_pompe_counter(66500)

pompe_monitor.register_pompe_callback(prototype_pompe_callback)
pompe_monitor.unregister_pompe_callback(prototype_pompe_remove_callback)
"""
