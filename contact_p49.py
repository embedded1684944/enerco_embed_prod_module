from auto import *
from pompe import *
from cycle import *
from mongodb import *
from kafka_prod import *
from contact_logs import *
import threading

class contacts:

    @staticmethod
    def on_change_callback(pack):
        print(pack)

    def __init__(self, cycle_log_dir, auto_log_dir, pompe_log_dir, kafka_log_dir, mongodb_log_dir, server_ip, mongodb_port, username_db:str, password_db:str, auth:bool, machine_id, kafka_topic):

        self.server_ip = server_ip
        self.machine_id = machine_id

        self.contact_pack = {
                            "id_machine": "u4d1m1", "action": {"in_production": None},
                             "state": {"pompe": None, "auto": None, "robot": 0}, "cycle": 0.0,
                             "datetime": get_time_date()
                             }

        self.mongodb_server = mongodb(self.server_ip, mongodb_port, username_db, password_db, auth, self.machine_id, mongodb_log_dir)
        self.kafka_prod = kafka_producer(server_ip, kafka_topic, kafka_log_dir)
        self.kafka_prod.create_kafka_producer()
        
        self.cycle_monitor = cycle_state(cycle_log_dir)
        self.cycle_monitor.start_point()

        self.auto_monitor = auto_state(auto_log_dir)
        
        self.pompe_monitor = pompe_state(pompe_log_dir)
        self.pompe_monitor.start_pompe_task()

        self.shutdown_condition = False
        
        self.contact_pack["state"]["pompe"] = self.pompe_monitor.pompe_state.get("state")
        self.contact_pack["state"]["auto"] = self.auto_monitor.auto_pack.get("auto_state")
        self.contact_pack["cycle"] = self.cycle_monitor.cycle_pack["cycle"]
        
        if self.cycle_monitor.cycle_pack["machine_cycle_state"] and self.auto_monitor.machine_auto_state and self.pompe_monitor.machine_pompe_state:
            self.contact_pack["action"]["in_production"] = True
        else:
            self.contact_pack["action"]["in_production"] = False
        
        self.pack_lock = threading.Lock()

        self.pompe_monitor.update_initial_pompe_counter(self.mongodb_server.get_latest_pompe_counter())
        #self.pompe_monitor.update_initial_pompe_counter(65000)
        self.pompe_monitor.pompe_pack.update({"id_machine":self.machine_id})
        self.callback = None
        print(self.contact_pack)
        self.start_cycle_task()
        
    def change_in_production_state(self, callback_type:str):
        if not self.contact_pack["action"]["in_production"]:
            if self.cycle_monitor.cycle_pack["machine_cycle_state"] and self.auto_monitor.machine_auto_state and self.pompe_monitor.machine_pompe_state:
                self.contact_pack["action"]["in_production"] = True
                print("From {} State Callback - True".format(callback_type))
                print(self.contact_pack)
        elif self.contact_pack["action"]["in_production"]:
            if not (self.cycle_monitor.cycle_pack["machine_cycle_state"] and self.auto_monitor.machine_auto_state and self.pompe_monitor.machine_pompe_state):
                self.contact_pack["action"]["in_production"] = False
                print("From {} State Callback - False".format(callback_type))
                print(self.contact_pack)
        
    def set_callback(self, callback):
        self.callback = callback
        
    def auto_callback(self, machine_auto_state, time_date):
        self.pack_lock.acquire()
        self.contact_pack["state"]["auto"] = self.auto_monitor.machine_auto_state
        self.contact_pack["datetime"] = time_date
        if self.contact_pack["state"]["auto"] == False:
            self.cycle_monitor.last_rising_edge_time = None
            self.cycle_monitor.cycle_pack["machine_cycle_state"] = False
            self.contact_pack["cycle"] = 0.0
        self.change_in_production_state("auto")
        self.pack_lock.release()
        
    def pompe_callback(self, pompe_pack, pompe_state_pack, pack_type:str):
        if pack_type == "pompe_pack":
            print(pompe_pack)
            self.kafka_prod.update_kafka_message(pompe_pack)
            self.kafka_prod.produce_message()
        elif pack_type == "pompe_state_pack":
            self.pack_lock.acquire()
            self.contact_pack["state"]["pompe"] = pompe_state_pack.get("state")
            self.contact_pack["datetime"] = pompe_state_pack.get("time_date")
            if self.contact_pack["state"]["pompe"] == False:
                self.cycle_monitor.last_rising_edge_time = None
                self.cycle_monitor.cycle_pack["machine_cycle_state"] = False
                self.contact_pack["cycle"] = 0.0
            self.change_in_production_state("pompe")
            self.pack_lock.release()
        else:
            print("ERROR: pompe_condition!") 

    def start_cycle_task(self):
        cycle_task_thread = threading.Thread(target=self.cycle_thread_task)
        cycle_task_thread.daemon = True  # Daemonize the thread so it stops when the main program ends
        cycle_task_thread.start()

    def cycle_thread_task(self):
        while True:
            current_time = float("{:.4f}".format(time.time()))
            if (self.contact_pack["state"]["pompe"] and self.contact_pack["state"]["auto"]) == True:
                if self.cycle_monitor.last_rising_edge_time is not None and self.cycle_monitor.cycle_pack["machine_cycle_state"]:
                    if (current_time - self.cycle_monitor.last_rising_edge_time) >= 60:
                        self.contact_pack["cycle"] = 0.0
                        self.contact_pack["datetime"] = get_time_date()
                        self.contact_pack["action"]["in_production"] = False
                        self.cycle_monitor.last_rising_edge_time = None
                        self.cycle_monitor.cycle_pack["machine_cycle_state"] = False
                        self.callback(self.contact_pack)
                        self.kafka_prod.update_kafka_message(self.contact_pack)
                        self.kafka_prod.produce_message()
            else:
                if self.shutdown_condition == False:
                    self.cycle_monitor.last_rising_edge_time = None 
                    self.cycle_monitor.cycle_pack["machine_cycle_state"] = False
                    self.contact_pack["cycle"] = 0.0
                    self.contact_pack["datetime"] = get_time_date()
                    self.contact_pack["action"]["in_production"] = False
                    print(self.contact_pack)
                    self.kafka_prod.update_kafka_message(self.contact_pack)
                    self.kafka_prod.produce_message()
                    #self.callback(self.contact_pack)
                    self.shutdown_condition = True

    def cycle_callback(self,cycle_pack):
        self.pack_lock.acquire()
        self.contact_pack["cycle"] = cycle_pack.get("cycle")
        self.contact_pack["datetime"] = cycle_pack.get("time_date")
        self.shutdown_condition = False 
        if not self.contact_pack["action"]["in_production"] and self.contact_pack["cycle"] == 0:
            self.cycle_monitor.cycle_pack["machine_cycle_state"] = False
        else:
            self.cycle_monitor.cycle_pack["machine_cycle_state"] = True
            self.contact_pack["action"]["in_production"] = True
            print(self.contact_pack)
            self.kafka_prod.update_kafka_message(self.contact_pack)
            self.kafka_prod.produce_message()
        self.pack_lock.release()
        
    
    def unregister_contact_callback(self):
        pass
        
prod_instance = contacts("/home/pi/Desktop/embedded_production_monitor/enerco_embed_prod_module/cycle_logs/",
                "/home/pi/Desktop/embedded_production_monitor/enerco_embed_prod_module/auto_logs/",
                "/home/pi/Desktop/embedded_production_monitor/enerco_embed_prod_module/pompe_logs/",
                "/home/pi/Desktop/embedded_production_monitor/enerco_embed_prod_module/kafka_logs/",
                "/home/pi/Desktop/embedded_production_monitor/enerco_embed_prod_module/mongodb_logs/",
                "192.168.2.63",
                "27017",
                "electrosoft_prod",
                "electrosoft@2023",
                True,
                "u4d1m1",
                "dataprod7")

prod_instance.set_callback(prod_instance.on_change_callback)

prod_instance.auto_monitor.register_machine_auto_state_callback(prod_instance.auto_callback)
prod_instance.auto_monitor.unregister_machine_auto_state_callback(prod_instance.unregister_contact_callback)

prod_instance.pompe_monitor.register_pompe_callback(prod_instance.pompe_callback)
prod_instance.pompe_monitor.unregister_pompe_callback(prod_instance.unregister_contact_callback)

prod_instance.cycle_monitor.register_cycle_callback(prod_instance.cycle_callback)
prod_instance.cycle_monitor.unregister_cycle_callback(prod_instance.unregister_contact_callback)


while True:
    time.sleep(10)
