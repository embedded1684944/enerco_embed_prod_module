import RPi.GPIO as GPIO

from contact_logs import *


class auto_state:

    def __init__(self, log_dir):

        self.auto_gpio = 5

        self.machine_auto_state = False
        self.time_date = get_time_date()

        self._machine_auto_state_callbacks = []

        self.auto_logger = contact_logger('auto_logger', log_dir)
        
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.auto_gpio, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(self.auto_gpio, GPIO.BOTH, callback=self.handle_auto, bouncetime=500)

        if GPIO.input(self.auto_gpio) == 1:
            self.machine_auto_state = True
        else:
            self.machine_auto_state = False
        self.auto_pack = {'auto_state': self.machine_auto_state , 'time_date': self.time_date}
        
        print("Initial Auto State : {}".format(self.machine_auto_state))
        self.auto_logger.logger_info("Initial Auto State : {}".format(self.machine_auto_state))

    def handle_auto(self, channel):
        """
        Callback Function for the selected GPIO
        Monitor auto relay if case of state change.
        Will update auto_pack dictionary with the corresponding state and time/date.
        :param channel: GPIO related to Auto Relay
        """
        self.time_date = get_time_date()
        self.machine_auto_state = GPIO.input(self.auto_gpio)

        for callback in self._machine_auto_state_callbacks:
            callback(self.machine_auto_state, self.time_date)

        self.auto_pack.update(
            {
                'auto_state': self.machine_auto_state,
                'time_date': self.time_date
            }
        )
        self.auto_logger.logger_info("Auto State : {}".format(self.auto_pack))

    def register_machine_auto_state_callback(self, callback):
        """
        Appends a callback function in the callback list once a change in auto state occurs.
        :param callback: User defined function (Register function)
        """
        self._machine_auto_state_callbacks.append(callback)

    def unregister_machine_auto_state_callback(self, callback):
        """
        Delete a callback function from the callback list if it exists.
        :param callback: User defined function (Unregister function)
        """
        if callback in self._machine_auto_state_callbacks:
            self._machine_auto_state_callbacks.remove(callback)


def new_auto_state_callback(machine_auto_state, date_time):
    state = "ON" if machine_auto_state else "OFF"
    print(f"Machine auto state: {state} at {date_time}")


def unregister_callback():
    pass

"""
log_dir = "/home/pi/Desktop/test_contact_sec/auto_logs/"

auto_monitor = auto_state(log_dir)

auto_monitor.register_machine_auto_state_callback(new_auto_state_callback)
auto_monitor.unregister_machine_auto_state_callback(unregister_callback)
"""
