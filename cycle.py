import RPi.GPIO as GPIO
import time
import threading
from contact_logs import *

class cycle_state:
    def __init__(self, log_dir):
        self.tc_gpio = 0
        self.last_rising_edge_time = None
        self.cycle_pack = {"cycle": 0.0, "machine_cycle_state": False, "time_date":get_time_date()}
        self.cycle_callback = []
        
        self.cycle_logger = contact_logger('cycle_logger', log_dir)      

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.tc_gpio, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(self.tc_gpio, GPIO.RISING, callback=self.handle_cycle, bouncetime=500)

        if GPIO.input(self.tc_gpio) == 1:
            self.last_rising_edge_time = float("{:.4f}".format(time.time()))
            self.cycle_pack["machine_cycle_state"] = True
        else:
            self.cycle_pack["machine_cycle_state"] = False
            self.last_rising_edge_time = None
        #self.start_point()
            
    def start_point(self):
        print("Awaiting Starting Point for Cycle at {}".format(get_time_date()))
        self.cycle_logger.logger_info("Awaiting Starting Point for Cycle ...")
        start_time = float("{:.4f}".format(time.time()))
        timer = 0
        while True:
            print("Current Timer: {} Seconds".format(timer))
            self.cycle_logger.logger_info("Current Timer: {} Seconds".format(timer))
            current_time = float("{:.4f}".format(time.time()))
            if (current_time - start_time) >= 60:
                print("Time is up at {}".format(get_time_date()))
                self.cycle_logger.logger_info("Time is up at {}".format(get_time_date()))
                break
            if self.cycle_pack["machine_cycle_state"]:
                print("Machine Cycle State is up with Timer: {} Seconds at {}".format(timer, get_time_date()))
                self.cycle_logger.logger_warning(
                    "Machine Cycle State is up with Timer: {} Seconds at {}".format(timer, get_time_date()))
                break
            timer += 1
            time.sleep(1)
        print("Starting Cycle Task at {}".format(get_time_date()))
        #self.start_cycle_task()
     
    def handle_cycle(self, channel):
        current_time = float("{:.4f}".format(time.time()))
        self.cycle_pack["machine_cycle_state"] = True
        self.cycle_pack["time_date"] = get_time_date()
        if self.last_rising_edge_time is not None:
            self.cycle_logger.logger_info("Rising Edge At: {}".format(self.cycle_pack.get("time_date")))
            time_since_last_edge = float("{:.4f}".format(current_time - self.last_rising_edge_time))
            if time_since_last_edge <= 60:
                self.cycle_pack["cycle"] = time_since_last_edge
            else:
                self.cycle_pack["cycle"] = 0.0
            self.cycle_logger.logger_info("New Cycle: {}".format(self.cycle_pack["cycle"]))
        else:
            self.cycle_logger.logger_info("Cycle Started Working At: {}".format(self.cycle_pack.get("time_date")))
            self.cycle_pack["cycle"] = 0.0
        for callback in self.cycle_callback:
            callback(self.cycle_pack)
        self.last_rising_edge_time = current_time
         
    def register_cycle_callback(self, callback):
        self.cycle_callback.append(callback)

    def unregister_cycle_callback(self, callback):
        if callback in self.cycle_callback:
            self.cycle_callback.remove(callback)

"""
def cycle_callback(cycle_pack):
    print(f"New cycle pack: {cycle_pack}")

def unregister_callback():
    pass
"""
"""
log_dir = "/home/pi/Desktop/cycle_logs"

cycle_monitor = cycle_state(log_dir)

cycle_monitor.register_cycle_callback(cycle_callback)
cycle_monitor.unregister_cycle_callback(unregister_callback)

while True:
    time.sleep(1)
"""

'''
--------------------------------------------------
'''
